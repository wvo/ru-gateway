[English](./README.md) | 简体中文

<p style="text-align: center">
    沙漠风
</p>
<h1 style="text-align: center">uc-gateway</h1>
<h3 style="text-align: center">:lemon: :tangerine: :cherries: :cake: :grapes: :watermelon: :strawberry: :corn: :peach: :melon:</h3>

<p style="text-align: center">
  <img alt="" src="https://img.shields.io/badge/license-MIT-brightgreen.svg">
</p>

	基于最新的reactjs生态系统的成熟的技术体系及强大的UI库Ant Design、简化繁琐的状态管理工具Dva搭建的一套开箱即用的前台后台管理系统，本项目是使用最新create-react-app脚手架搭建的目录架构，目录结构做了优化调整增加权限管理等内容 正在开发中后续再补充... 



## 目录
* 正在开发中后续再补充...

## 功能
* 正在开发中后续再补充...

## 工程结构
```
.
├── public                   # 不参与编译的资源文件
├── src                      # 主程序目录
│   ├── index.js             # 程序启动和渲染入口文件
│   ├── config.js            # 全局配置
│   ├── components           # 全局公共组件
│   ├── locale               # 国际化目录
│   ├── models               # dva model
│   ├── power                # 权限配置管理
│   ├── router               # 路由配置文件
│   ├── layouts              # 页面结构组件
│   │   ├── backstageLayout  # 前台界面布局
│   │   └── receptionLayout  # 后台界面布局
│   ├── views                # 动态路由目录（每个功能一个文件夹的MVC结构）
│   │   ├── defaultView      # 前台页面功能模块
│   │   └── SystemConsole    # 后台界面功能模块
│   ├── utils                # 工具类
│   │   ├── index            # 该目录的工具类都通过index引出被使用
│   │   └── request          # 前后台接口发送统一处理文件
│   └── assets               # 资源文件
│           ├── fonts        # 字体 & 字体图标
│           ├── images       # 图片
│           └── styles       # 全局样式
```

## 使用方法

``` javascript
$ git clone https://gitee.com/shangyunkeji/reactTodomvc.git
$ cd uc-epde
// 安装依赖
$ yarn install
// 启动
$ yarn start
// 打包
$ yarn build
// 打包带图形化分析
$ yarn build --analyze
```

idea EsLint
//抑制检出下一行语句
1.// noinspection JSUnresolvedFunction
2.// noinspection ES6ModulesDependencies


## 兼容性

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE / Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Opera" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Opera |
| --------- | --------- | --------- | --------- | --------- | 
| >= IE10 | last 2 versions | last 2 versions | last 2 versions | last 2 versions




