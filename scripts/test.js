'use strict';

// Do this as the first thing so that any code reading it knows the right env.
// noinspection ES6ModulesDependencies,ES6ModulesDependencies
process.env.BABEL_ENV = 'test';
// noinspection ES6ModulesDependencies,ES6ModulesDependencies
process.env.NODE_ENV = 'test';
// noinspection ES6ModulesDependencies
process.env.PUBLIC_URL = '';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
// noinspection ES6ModulesDependencies,ES6ModulesDependencies
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
require('../config/env');


const jest = require('jest');
// noinspection JSUnresolvedVariable
const execSync = require('child_process').execSync;
// noinspection ES6ModulesDependencies,ES6ModulesDependencies
let argv = process.argv.slice(2);

function isInGitRepository() {
  try {
    execSync('git rev-parse --is-inside-work-tree', { stdio: 'ignore' });
    return true;
  } catch (e) {
    return false;
  }
}

function isInMercurialRepository() {
  try {
    execSync('hg --cwd . root', { stdio: 'ignore' });
    return true;
  } catch (e) {
    return false;
  }
}

// Watch unless on CI or explicitly running all tests
// noinspection ES6ModulesDependencies,JSUnresolvedVariable
if (
  !process.env.CI &&
  argv.indexOf('--watchAll') === -1 &&
  argv.indexOf('--watchAll=false') === -1
) {
  // https://github.com/facebook/create-react-app/issues/5210
  const hasSourceControl = isInGitRepository() || isInMercurialRepository();
  argv.push(hasSourceControl ? '--watch' : '--watchAll');
}


// noinspection JSIgnoredPromiseFromCall
jest.run(argv);
