import { Map, fromJS } from 'immutable';
import { routerRedux } from 'dva/router';
import config from '../router/config.js';
const { menuGlobal } = config;

//初始数据
const initState = Map({
    //i18n: 'zh_CN',
    token: null,
    locationPathname: null,
    menu: menuGlobal
});

// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
export default {
    // 用作顶层state key，以及action前缀
    namespace: 'app',
    // module级初始state
    state: initState,
    // 订阅其它数据源，如router change，window resize, key down/up...
    subscriptions: {
        //监听路由
        setup({ dispatch, history }) {

        },
        setupHistory({ dispatch, history }) {
            history.listen((location) => {
                dispatch({
                    type: 'updateLocation',
                    payload: {
                        locationPathname: location.pathname
                    },
                });
                dispatch({
                    type: 'updateToken',
                    payload: {
                        token: window.sessionStorage.getItem('token')
                    },
                })
            })
        },
    },
    //异步操作
    effects: {

        * changeLang({
            payload: { value },
        }, { put }) {
            yield put({ type: 'updateLang', payload: { value } });
        },

        * updateLocation({
            payload
        }, { put, select }) {
            yield put({ type: 'updateStore', payload });
        },

        * updateToken({
            payload
        }, { put, select }) {
            yield put({ type: 'updateStore', payload });
        },

        * goToPage({
            payload
        }, { put, select }) {
            yield put(routerRedux.push({
                pathname: payload.url
            }));
        },

        * goBack({
            payload
        }, { put, select }) {
            yield put(routerRedux.goBack());
        },

        * loginOk({
            payload
        }, { put, select }) {
            //const musicUrl = yield select(_ => _.app.getIn(['menu', 'byId', 'music', 'path']))
            const bs_IndexUrl = "/systemConsole/index";
            window.sessionStorage.setItem('token', payload.token);
            yield put(routerRedux.push({
                pathname: bs_IndexUrl
            }));
        },

        * logout({
            payload
        }, { put, select }) {
            window.sessionStorage.removeItem('token');
            yield put(routerRedux.push('/systemConsole/login'));
            // yield put(routerRedux.replace('/'));
            // yield put(routerRedux.replace({
            //     pathname: '/dashboard/BaseInstance',
            //     state: { // 标识
            //         id: '0B64AF10-F1D0-6CD0-647F-160C50326F9D',
            //     },
            // }));
            //window.location.href = '/login';
        },

    },
    //同步操作
    reducers: {
        updateLang(state, { payload: { value } }) {
            return state.set('i18n', value);
        },
        updateStore(state, { payload }) {
            return payload ? state.mergeDeep(fromJS(payload)) : initState
        },

    },

};
