import React from 'react';
import { Layout, Dropdown, Menu, Icon,Button,message } from 'antd';
import styles from './index.module.less';
import {connect} from "dva";
import { Link } from 'dva/router';

// noinspection SpellCheckingInspection
const { Header, Sider, Content } = Layout;

class ConsoleLayout extends React.Component {
    state = {
        collapsed: false,
    };

    //react生命周期函数
    componentDidMount() {
        //console.log("url传参",this.props.match.params);
    }

    //展开及关闭左侧菜单
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    //测试
    handleTest =()=> {
        message.info("建设中。。。")
    };

    //退出系统
    handleLogout = () => {
        const { dispatch } = this.props;
        dispatch({
            type: 'app/logout'
        })
    };

    render() {

        //用户个人信息菜单
        const menu = (
            <Menu>
                <Menu.Item key="0" onClick={this.handleTest}>
                    <span>
                        个人主页
                    </span>
                </Menu.Item>
                <Menu.Item key="1" onClick={this.handleTest}>
                    <span>
                        设置
                    </span>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="2" onClick={this.handleTest}>
                    帮助
                </Menu.Item>
                <Menu.Item key="3" onClick={this.handleLogout} >
                    退出
                </Menu.Item>
            </Menu>
        );

        //---
        return (
            <Layout className={styles.consoleIndex}>
                <Sider trigger={null} collapsible collapsed={this.state.collapsed} className={this.state.collapsed?styles.closeSide:styles.openSide}>
                    <div className={styles.consoleLogo} > 控制台中心 </div>
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1">
                            <Link to={"/systemConsole/index"} >
                                <Icon type="control" />
                                <span>控制台首页</span>
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Link to={"/systemConsole/user/index"} >
                                <Icon type="video-camera" />
                                <span>todoMVC</span>
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                        <div className={styles.navUserBar}>
                            <Dropdown overlay={menu}
                                      placement={"bottomCenter"}
                                      //mouseLeaveDelay={5000}   //鼠标移出后延时多少才隐藏 Tooltip，单位：秒 0.1
                            >
                                <span className="ant-dropdown-link" >
                                    <Button className={styles.userIco} type="primary" shape="circle" icon="user" size={"large"} />
                                    <Icon type="down" />
                                </span>
                            </Dropdown>
                        </div>
                    </Header>
                    <Content
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            background: '#fff',
                            minHeight: 280,
                        }}
                    >
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

//export default BackstageLayout;
//使用公共state
function mapStateToProps(state) {
    return {
        user: state.app
    };
}

//使用公共方法
// function mapDispatchToProps(dispatch) {
//     return {
//         actions: bindActionCreators(LoginActions, dispatch),
//         routerActions: bindActionCreators({ pushState }, dispatch)
//     }
// }

export default connect(mapStateToProps)(ConsoleLayout);