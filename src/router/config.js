const menuGlobal = [
    //前台部分路由
    {
        id: 'home',
        pid: '0',
        name: '首页',
        icon: 'home',
        display: 'block',
        path: '/',
        models: () => { return ''; }, //models可多个
        component: () => { return import('../views/defaultView/home'); },
    },{
        id: 'type1',
        pid: '0',
        name: '子页面',
        icon: 'type1',
        display: 'block',
        path: '/childrenPage/:level1/:level2',
        models: () => { return ''; }, //models可多个
        component: () => { return import('../views/defaultView/childrenPage'); },
    },
    // 后台部分路由
    {
        id: 'login',
        pid: '0',
        name: '登录',
        icon: 'user',
        display: 'block',
        path: '/systemConsole/login',
        models: () => [import('../views/SystemConsole/login/models')], //models可多个
        component: () => import('../views/SystemConsole/login'),
    },
    {
        id: 'systemConsole',
        pid: '0',
        name: '系统控制台默认页',
        icon: 'user',
        display: 'block',
        path: '/systemConsole/index',
        models: () => [import('../views/SystemConsole/index/models'),import('../views/SystemConsole/pages/usersManage/models')], //models可多个
        component: () => import('../views/SystemConsole/index'),
    },{
        id: 'type1',
        pid: '0',
        name: '系统控制台子页面',
        icon: 'type1',
        display: 'block',
        path: '/systemConsole/:level1/:level2',
        models: () => [import('../views/SystemConsole/pages/usersManage/models')], //models可多个
        component: () => { return import('../views/SystemConsole/index'); },
    },
];

//输出
export default {
    menuGlobal
}