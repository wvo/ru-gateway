import React from 'react';
import { Router, Route, Switch, Redirect } from 'dva/router';
import dynamic from 'dva/dynamic';
//权限管理
import Power from '../power';
//工具类
import config from './config.js';
const { menuGlobal } = config;

//React-router重定向
//1、<Redirect> 标签  如：<Redirect to={'/default'}/>
//  或者//<Redirect
//     to={{
//         pathname: "/login",
//         search: "?utm=your+face",
//         state: { referrer: currentLocation }
//     }}
// />
//2、【编程式导航方式】 如：this.props.history.push('/default')


function RouterConfig({ history, app }) {
    const error = dynamic({
        app,
        component: () => import('./../views/defaultView/noMatch'),
    });
    return (
        <Router history={history}>
            <Power>
                <Switch>
                    {/* 测试使用（正式环境去掉或者删除） */}
                    {/*<Route exact path="/" render={() => { return <Redirect to="/systemConsole/login" /> }} />*/}
                    {/* 核心路由解析 */}
                    {
                        menuGlobal.map(({ path, ...dynamics }, index) => (
                            <Route
                                key={index}
                                path={path}
                                exact
                                component={dynamic({
                                    app,
                                    ...dynamics
                                })}
                            />
                        ))
                    }
                    {/* 无对应路由时 指向 */}
                    <Route component={error} />
                    <Redirect from="/*" to="/" />  {/*   Redirect 必须放在最后一行*/}
                </Switch>
            </Power>
        </Router >
    );
}

export default RouterConfig;
