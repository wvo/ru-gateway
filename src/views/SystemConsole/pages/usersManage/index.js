import React from 'react';
import { connect } from 'dva';
import {Table, Pagination, Popconfirm, Button,Skeleton, message,notification,Icon} from 'antd';
// import { routerRedux } from 'dva/router';
import styles from './index.module.less';
import UserModal from './modal';

class UserManage extends React.Component {
  constructor(props){
    super(props);
    this.state={
      loading:true,
    }
  }

  //react
  componentDidMount() {

    //加载数据
    this.queryHandler();

  }

  // componentWillReceiveProps(nextProps, nextContext) {
  //   message.warn("神奇的。。。。。");
  // }

  //查询数据列表
  queryHandler =()=> {
    const { dispatch } = this.props;
    dispatch({
      type: 'usersManage/fetchUserInfo',
      payload: { page:1,limit:10 },
      callback: () => {
        //debugger
        this.setState({
          loading:false,
        })
        //this.openNotification(data.code,data.msg);
      }
    });
  };

  //删除方法
  deleteHandler =(id)=> {
    const { dispatch } = this.props;
    dispatch({
      type: 'usersManage/remove',
      payload: {
        id
      },
      callback: (data) => {
        this.openNotification(data.code,data.msg);
      }
    });
  };

  //监听翻页
  pageChangeHandler =(page)=> {
    const { dispatch } = this.props;
    dispatch({
      type: 'usersManage/fetchUserInfo',
      payload: { page,limit:10 }
    });
    /*dispatch(
        routerRedux.push({
          pathname: '/users',
          query: { page },
        })
    );*/
  };


  //编辑方法
  editHandler =(e,{values})=> {
    const { dispatch } = this.props;
    dispatch({
      type: 'usersManage/patch',
      payload: {
        ...e,
        ...values,
      },
      callback: (data) => {
        this.openNotification(data.code,data.msg);
      }
    });
  };

  //创建方法
  createHandler =({values})=> {
    const { dispatch } = this.props;
    dispatch({
      type: 'usersManage/createUser',
      payload: values,
      callback: (data) => {
          this.openNotification(data.code,data.msg);
      }
    })
  };


  //通知提醒
  openNotification = (type,description) => {
    notification.open({
      message: "通知提醒",
      description:description,
      icon: (type==="1"?
          <Icon type="check-circle" style={{color:"green"}} />:
          (type==="0"?<Icon type="close-circle" style={{color:'red'}} />:<Icon type="info-circle" style={{color:"yellow"}} />
          )),
    });
  };


  //UI渲染
  render() {

    const { dataSource,loading,total,current } = this.props;
    // noinspection JSUnresolvedVariable,JSUnusedGlobalSymbols
    const columns = [
              {
                title: '人员编码',
                dataIndex: 'userCode',
                key: 'userCode',
                sorter: (a, b) => a.userCode - b.userCode,
                render: text => <span >{text}</span>,
              },
              {
                title: '姓名',
                dataIndex: 'userName',
                key: 'userName',
              },
              {
                title: '机构',
                dataIndex: 'organization',
                key: 'organization',
              },
              {
                title: '学历',
                dataIndex: 'education',
                key: 'education',
              },
              {
                title: '职务',
                dataIndex: 'duties',
                key: 'duties',
              },
              {
                title: '操作',
                key: 'operation',
                render: (text, record) => (
                    <span className={styles.operation}>
                  <UserModal record={record} onOk={this.editHandler.bind(this,record)}>
                    <Button type="primary" size="small" className={styles.controlBtn}>编辑</Button>
                  </UserModal>
                  <Popconfirm
                      title="确定删除?"
                      onConfirm={this.deleteHandler.bind(this,record.id)}
                  >
                    <Button type="danger" size="small" className={styles.controlBtn}>删除</Button>
                  </Popconfirm>
                </span>
                ),
              },
     ];

    //---
    return (
        <Skeleton active loading={this.state.loading}>
          <div className={styles.ruComponent} >
            <div className={styles.normal}>
              <div>
                <div className={styles.create}>
                  <UserModal record={{}} onOk={this.createHandler} >
                    <Button type="primary"  >添加数据</Button>
                  </UserModal>
                </div>
                <Table
                    columns={columns}
                    dataSource={dataSource}
                    loading={loading}
                    rowKey={record => record.id}
                    pagination={false}
                />
                <Pagination
                    className="ant-table-pagination"
                    total={total}
                    current={current}
                    //pageSize={pageSize}
                    onChange={this.pageChangeHandler}
                />
              </div>
            </div>
          </div>
        </Skeleton>
    );
  }

}


const mapStateToProps =(state)=>{
  try {
    const {usersManage} = state;
      const { list, total, page } = usersManage;
      return {
        dataSource:list,
        total:total,
        current:page,
      };
    }catch (e) {
      message.info(e.toString());
      return {}
    }
};

export default connect(mapStateToProps)(UserManage);