import { request } from '../../../../../utils';


/**
 * 作用：创建用户
 **/
export async function createUser(params) {
  return request('/app/insert', {
    method: 'POST',
    //body: JSON.stringify(values),
    body: {
      ...params
    }
  })
}

/**
 * 作用：删除
 **/
export function remove(payload) {
  return request(`/app/delete`, {
    method: 'DELETE',
    body: {
       ...payload
    },
  });
}

/**
 * 作用：修改
 **/
export function patch(params) {
  return request(`/app/update`, {
    method: 'PUT',
    //body: JSON.stringify(values),
    body: {
      ...params
    }
  });
}


/**
 * 作用：查询
 **/
export async function fetchUserInfo(payload) {
  return request("/app/queryBatch", {
    method: 'GET',
    // body: JSON.stringify(payload),
    params: {
      ...payload
    }
  })
}
