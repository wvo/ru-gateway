import * as usersService from '../services';
import { message } from 'antd';

// noinspection JSUnusedGlobalSymbols,JSUnusedGlobalSymbols,JSUnusedGlobalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols
export default {
  //命名空间
  namespace: 'usersManage',
  //状态数据
  state: {
    list: [],
    total: 0,
    page: 0,
    limit:10,
  },
  //异步方法
  effects: {
    //----
    *fetchUserInfo({ payload, callback }, { call, take, select, put }) {
      //let dse_sessionId= yield select(state => state.currentUser.sessionId),
      try {
        const data = yield call(usersService.fetchUserInfo, payload);
        yield put({
          type: 'setUserInfo',
          payload: {
            ...data.data
          }
        });
        callback(data);
      } catch (err) {
        //message.error("查询："+JSON.stringify(err));
      }
    },
    *remove({ payload,callback }, { call, put }) {
      try {
        const data = yield call(usersService.remove, payload);
        yield put({ type: 'reload' });
        callback(data);
      } catch (err) {
        message.error("删除:"+JSON.stringify(err));
      }
    },
    *patch({ payload, callback }, { call, put }) {
      try {
      const data=yield call(usersService.patch, payload);
      yield put({ type: 'reload' });
      callback(data);
      } catch (err) {
        message.error("修改:"+JSON.stringify(err));
      }
    },
    *createUser({ payload, callback }, { call, take, select, put }) {
      //let dse_sessionId= yield select(state => state.currentUser.sessionId),
      try {
        const data = yield call(usersService.createUser, payload);
        yield put({ type: 'reload' });
        callback(data);
      } catch (err) {
        message.error("创建:"+JSON.stringify(err));
      }
    },
    *reload(action, { put, select }) {
      // noinspection JSUnresolvedVariable
      const {page ,limit } = yield select(state => state.usersManage);
      yield put({ type: 'fetchUserInfo', payload: { page,limit } });
    },
  },
  //同步方法
  reducers: {
    save(state, { payload: { data: list, total, page } }) {
      return { ...state, list, total, page };
    },
    setUserInfo(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  //初始化时执行
  subscriptions: {
    setup({ dispatch, history }) {
      //--
      // noinspection JSUnusedLocalSymbols,JSUnusedLocalSymbols
      return history.listen(({ pathname, query }) => {
        //debugger
        // if (pathname === '/systemConsole/user/index') {
        //   dispatch({ type: 'fetchUserInfo', payload: { page:1,limit:10 } });
        // }
      });
    },
  },
};
