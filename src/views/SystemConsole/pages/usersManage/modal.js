import React, { Component } from 'react';
import { Modal, Form, Input, InputNumber } from 'antd';

const FormItem = Form.Item;

class UserEditModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  showModelHandler = e => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
    });
  };

  okHandler = () => {
    const { onOk } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        onOk({values});
        this.hideModelHandler();
      }
    });
  };

  render() {
    const { children } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { userCode, userName, organization, education, duties } = this.props.record;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    return (
      <span>
        <span onClick={this.showModelHandler}>{children}</span>
        <Modal
          title="编辑用户信息"
          visible={this.state.visible}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
        >
          <Form layout="horizontal" onSubmit={this.okHandler}>
            <FormItem {...formItemLayout} label="人员编码">
              {getFieldDecorator('userCode', {
                initialValue: userCode,
                rules: [{ required: true, message: '人员编码 为必填项!' }],
              })(<InputNumber style={{width:"100%"}} placeholder={"该项只能输入数字类型的数据"}  min={1} max={10000000000000000000}/>)}
            </FormItem>
            <FormItem {...formItemLayout} label="姓名">
              {getFieldDecorator('userName', {
                initialValue: userName,
              })(<Input />)}
            </FormItem>
            <FormItem {...formItemLayout} label="所在机构">
              {getFieldDecorator('organization', {
                initialValue: organization,
              })(<Input />)}
            </FormItem>
            <FormItem {...formItemLayout} label="学历">
              {getFieldDecorator('education', {
                initialValue: education,
              })(<Input />)}
            </FormItem>
            <FormItem {...formItemLayout} label="职务">
              {getFieldDecorator('duties', {
                initialValue: duties,
              })(<Input />)}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(UserEditModal);
