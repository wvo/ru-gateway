import React, { Component } from 'react';
import { connect } from 'dva'
import { Row, Col, Form, Icon, Input, Button } from 'antd'
import styles from './index.module.less';

const FormItem = Form.Item;

class Login extends Component {

    loginSubmit = (e) => {
        e.preventDefault();
        const { form, dispatch } = this.props;
        form.validateFields((err, values) => {
            if (!err) {
                dispatch({
                    type: 'login/login',
                    payload: {
                        values
                    }
                })
            }
        });
    };

    render() {

        const { form } = this.props;
        const { getFieldDecorator } = form;
        return (
            <div className={styles.loginContent}>
                <div className={styles.loginMain}>
                    <Row>
                        <Col className={styles.loginFormCol}>
                            <Form onSubmit={this.loginSubmit} className={styles.loginForm}>
                                <div  className={styles.loginTitle}>
                                    <h3 className={styles.titleText}>登录</h3>
                                    <a className={styles.titleReturnIndex} href={"#/"}>返回首页</a>
                                </div>
                                <FormItem>
                                    {getFieldDecorator('username', {
                                        rules: [{
                                            required: true,
                                            message: "请输入用户名"
                                        }],
                                    })(
                                        <Input className={styles.userInput} autoComplete={'off'} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="username(admin)" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator('password', {
                                        rules: [{
                                            required: true,
                                            message: '请输入密码'
                                        }],
                                    })(
                                        <Input className={styles.userInput} autoComplete={'off'} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="password(123456)" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    <Button type="primary" htmlType="submit" className={styles.loginBtn}>
                                        登录
                                    </Button>
                                </FormItem>
                            </Form>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

export default connect(({
    app,
}) => ({
    app,
}))(Form.create()(Login))