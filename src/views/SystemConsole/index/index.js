import React from 'react';
import { Spin } from 'antd';
import { isEmpty } from "../../../utils/bese";

import Index from './../pages/Default';  //系统首页
import UserManage from './../pages/usersManage';  //用户管理页

import styles from './index.module.less';  //样式当前组件样式

class IndexPage extends React.Component {
    state = {
        view:"",   //要展示的页面UI
        loading: true,   //加载状态
    };

    //react生命周期函数
    componentDidMount() {
        let seePage = "";
        const { params } = this.props.match;
        if (!isEmpty(params)){
            let { level1 } = params;
            switch (level1) {
                case "user":
                    seePage=<UserManage />;
                    break;
                case "userManage":
                    seePage=<UserManage />;
                    break;
                default :
                    seePage = <div/>;
                    break
            }
        }else{
            seePage=<Index />
        }
        this.setState({view:seePage},()=>{
            setTimeout(()=>{
                this.setState({loading:false});
            },200);
        });
        //console.log("url传参111",this.props.match.params);
    }


    render() {

        const { loading } = this.state;

        return (
            <div className={styles.consoleIndex}>
                <Spin size="large" spinning={loading} >
                {/*<Skeleton active loading={this.state.loading}>*/}
                    {this.state.view}
                {/*</Skeleton>*/}
                </Spin>
            </div>
        );
    }
}

export default IndexPage;