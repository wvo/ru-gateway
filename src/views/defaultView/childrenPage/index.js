import React from 'react';
import styles from './index.module.less';
import commonStyles from './../../../assets/styles/common.module.less';


//默认参数类型
// const propTypes = {
//     text: PropTypes.string,
// };

//默认数据
const defaultProps = {
    text: '通用子模块',
};

//前台主页
class IndexPage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    //react生命周期函数
    componentDidMount() {
        console.log("url传参",this.props.match.params);
        // 第一种方式跳转
        // <a href='#/skinCare/3'>去skinCare 3</a>
        // 第二种方式跳转
        //     <button onClick={() => this.props.history.push({
        //         pathname: '/skinCare',
        //         state: {
        //             id: 3
        //         }
        // })}>通过函数跳转</button>
    }

    //UI渲染
    render() {
        //---
        return (
            <div className={styles.ruComponent + ' ' + commonStyles.baseContentDefault}>
                <div className={commonStyles.content}>
                    <h1 className={"bottom "+ styles.textOne} >{this.props.text}</h1>
                    <h1 className={"bottom "+ styles.textTwo} >{this.props.text}</h1>
                </div>
            </div>
        );

    }
}

//传入参数类型验证 关联 当前组件
//IndexPage.propTypes = propTypes;

//未获取到参数时赋默认值 关联 当前组件
IndexPage.defaultProps = defaultProps;

//输出组件
export default IndexPage;