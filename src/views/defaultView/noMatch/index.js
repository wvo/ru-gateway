import React from 'react';
import styles from './index.module.less';

class NoMatch extends React.Component {

    render() {
        return (
            <div className={styles.noMatchBody} >
                <div className={styles.noMatchBodyMain}>
                    <div>
                        <p>404 Page !!!</p>
                        <p style={{ fontSize: '14px' }}>{window.navigator.userAgent}</p>
                    </div>
                </div>
            </div>

        );
    }

}

export default NoMatch;