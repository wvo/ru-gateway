// import {connect} from 'dva';
import React from 'react';
import Helmet from 'react-helmet';
import Header from './../../components/rcs-reception-header';
import Footer from './../../components/rcs-reception-footer';
import styles from './index.module.less';

const IndexPage = ({ children, dispatch, menu, locationPathname, }) => {
  //当前菜单位置
  let menuName = '信息展示';

  const headerProps = {
    children, dispatch, menu, locationPathname,
  };

  return (
    <React.Fragment>
      <Helmet>
        <title>
          {menuName}
        </title>
      </Helmet>

      <div className={styles.ruLayout}>
        <div className={styles.ruLayoutHeader}>
          <Header {...headerProps} />
        </div>
        <div className={styles.ruLayoutContent}>
          <div className={styles.ruLayoutContentMain}>
            {children}
          </div>
        </div>
        <div className={styles.ruLayoutFooter}>
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
};

export default IndexPage
