import React from 'react';
import Helmet from 'react-helmet';
import ConsoleLayout from './../../components/rcs-backstage-layout'

//提示执行性能react高级语法
const IndexPage = ({ children, dispatch, match, menu, locationPathname,token }) => {
  //当前菜单位置
  let menuName = '控制台中心';

  //判断是否是登录页，登录页面和内页是不同的布局(true:是，false:否)
    const isLoginPage = (token === null);
  //const isLoginPage = loginUrl === locationPathname ? true : false;

  //提供传入后台模板组件的数据
  const contentProps = {
    menu,
    match,
    locationPathname,
    dispatch
  };

  return (

    <React.Fragment>
       <Helmet>
          <title>
            {menuName}
          </title>
       </Helmet>
      {isLoginPage ?
        children
        :
          <ConsoleLayout {...contentProps}>
              {children}
          </ConsoleLayout>
      }
    </React.Fragment>
  );
};

export default IndexPage
