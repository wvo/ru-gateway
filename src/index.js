// 兼容ie11
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
// 使用dva集成react-redux,react-router,react-dom的来解决特别复杂的状态管理
import dva from 'dva';
import './assets/styles/bese.css';

//路由展示方法：import { createBrowserHistory } from 'history' 或import { createHashHistory } from 'history'
import { createHashHistory } from 'history';

// 1. Initialize
const app = dva({
    history: createHashHistory()
});

// 2. Plugins
// app.use({});

// 3. Model
// noinspection JSUnresolvedFunction
app.model(require('./models/app').default);

// 4. Router
// noinspection JSUnresolvedFunction
app.router(require('./router').default);

// 5. Start
app.start('#root');

// 模块热替换的 API
// noinspection JSUnresolvedVariable
if (module.hot) {
    module.hot.accept();
}
