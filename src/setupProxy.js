// noinspection JSUnresolvedFunction
const proxy = require('http-proxy-middleware');
//多级代理
module.exports = function (app) {
    // ...在这里可以根据需要注册代理
    //后台接口
    app.use(proxy('/app', {
        target: 'http://127.0.0.1:83',
        changeOrigin: true,
    }));
    //接口代理二
    // app.use(proxy('/apc', {
    //     target: 'http://172.19.5.34:9531//1/106..52.255.25/15',
    //     secure: false,
    //     changeOrigin: true,
    //     pathRewrite: {
    //         "^/apc": "/"
    //     },
    // }));
    //app.use(proxy('/apc', { target: 'http://172.19.5.34:9531' }));
};
