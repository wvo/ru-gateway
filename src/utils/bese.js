//export const router = myRouter

/**
 * 判断一个变量是否等于 空{} 或 空[]
 * @param   {array|object}     obj     传入的数组或对象
 * @return  {boolean}              返回一个布尔值（true:为空{}或空[],false:不为空）
 */
export function isEmpty(obj) {
    for(let prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    if (JSON.stringify(obj) === JSON.stringify({})){
        return true;
    }else { // noinspection RedundantIfStatementJS
        if(JSON.stringify(obj) === JSON.stringify([])){
                return true;
            }else {
                return false;
            }
    }
}

// noinspection JSUnusedGlobalSymbols
/**
 * 判断当前客户端浏览器是否支持css3（可以判断浏览器是否太低邦本）
 * 用法：检查是否支持transform  support_css3('transform')
 * @return  {boolean}              返回一个布尔值（true:支持css3,false:不支持css3）
 */
export const support_css3 = (function() {
    let div = document.createElement('div'),
        vendors = 'Ms O Moz Webkit'.split(' '),
        len = vendors.length;

    return function(prop) {
        if ( prop in div.style ) return true;

        prop = prop.replace(/^[a-z]/, function(val) {
            return val.toUpperCase();
        });

        while(len--) {
            if ( vendors[len] + prop in div.style ) {
                return true;
            }
        }
        return false;
    };
})();



//判断网络是否连接方法（只兼容部分浏览器）
/*window.ononline = function() {
    alert("链接上网络了");
}
window.onoffline = function() {
    alert("网络链接已断开");
}*/

/**
 * 指定服务器地址资源（判断是否链接）
 * 这种方式会消耗网站的流量
 * @returns {Promise<unknown>}
 */
export function onLine(){//基于promise，可以用async和await
    let img = new Image();
    img.src = 'https://www.baidu.com/favicon.ico?_t=' + Date.now();
    // noinspection JSUnusedLocalSymbols
    return new Promise((resolve,reject)=>{
        img.onload=function(){
            resolve(true)
        };
        img.onerror=function(){
            resolve(false)
        };
    })
}
// //在异步函数可以直接await（执行：toServiceOnLine() 即可）
// noinspection JSUnusedGlobalSymbols
export async function toServiceOnLine() {
    let net_state = await onLine();
    net_state ? alert('服务器链接畅通') : alert('服务器链接中断');
}
