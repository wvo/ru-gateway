import { connect } from 'dva';
import React from 'react';
import BackstageLayout from './../layouts/backstageLayout';     //后台通用布局
import ReceptionLayout from './../layouts/receptionLayout';     //前台通用布局
import { withRouter } from 'dva/router';
//let browseType;  //false:浏览前台，true:浏览后台

//react高级语法提升处理性能
const Auth = ({ children, dispatch, token, locationPathname, menu, browseType,match }) => {

    if (locationPathname.indexOf("/systemConsole") < 0) {
        //用户访问前台内容
        browseType = false;
    } else {
        browseType = true;
        //用户访问后台管理内容
        if (!token && locationPathname !== '/systemConsole/login') {
            dispatch({
                type: 'app/logout'
            })
        } else if (token && locationPathname === '/systemConsole/login') {
            dispatch({
                type: 'app/loginOk',
                payload: {
                    token: token
                }
            })
        }
    }

    const layoutProps = {
        token,
        locationPathname,
        menu,
        dispatch,
        match
    };

    return (
        <React.Fragment>
            {
                browseType
                    ?
                    < BackstageLayout {...layoutProps}>
                        {children}
                    </BackstageLayout >
                    :
                    < ReceptionLayout {...layoutProps}>
                        {children}
                    </ReceptionLayout >
            }
        </React.Fragment>
    );
};

export default withRouter(connect(({
    app
}) => ({
    token: app.get('token'),
    locationPathname: app.get('locationPathname'),
    menu: app.get('menu'),
}))(Auth))
